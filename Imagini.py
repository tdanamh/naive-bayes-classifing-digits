# img 28x28 -> vector (784, )
# 1000 exemple
#  P(c/X) = ( P(X/c)*P(c) ) / P(X) Reg Bayes
# punem pixelii de la 0,1,2,....,784 intr-o histograma (care spune de cate ori a aparut pixelul intr-un interval)
# normalizam
# un pixel 0,255(intensitate)

import numpy as np
import matplotlib.pyplot as plt

train_images = np.loadtxt("data//train_images.txt")
train_labels = np.loadtxt("data//train_labels.txt", "int8")
test_images = np.loadtxt("data//test_images.txt")
test_labels = np.loadtxt("data//test_labels.txt", "int8")

#plot the first 100 testing images with their labels in a 10 x 10 subplot
#print("\nThe first 100 labels for testing images")
# nbImages = 10
# plt.figure(figsize=(5,5))
# for i in range(nbImages**2):
#     plt.subplot(nbImages,nbImages,i+1)
#     plt.axis('off')
#     plt.imshow(np.reshape(test_images[i,:],(28,28)),cmap = "gray")
# plt.show()
# labels_nbImages = test_labels[:nbImages**2]
#print(np.reshape(labels_nbImages,(nbImages,nbImages)))


# calcul apriori P(c = i)

p_C = np.zeros(10, 'uint8')
for label in train_labels:
    p_C[label] += 1

p_C = p_C/sum(p_C)
# print(p_C)


# calculam P(Xj = xj |c = i)

c = 0
indecsii_imaginilor_clasei_0 = np.ravel(np.where(train_labels == 0))
# print(indecsii_imaginilor_clasei_0)

indicele_pixel = 370
valori_pixel_370 = train_images[indecsii_imaginilor_clasei_0, indicele_pixel]
# print(valori_pixel_370)

vector_frecventa_valori_pixeli = np.bincount(valori_pixel_370.astype(int))
# print(vector_frecventa_valori_pixeli)

# nu ne ajuta, asa ca facem histograma, impartim intervalul 0,255 in 4
# histograma spune de cate ori am avut pixelul 370 cu valoarea in intervalul respectiv
numar_intervale = 4
limite_interval = np.linspace(0, 256, numar_intervale+1)
# print(limite_interval)
histogram, useless = np.histogram(valori_pixel_370, limite_interval)
# print(histogram/sum(histogram))

# histogramele pt valorile tuturor pixelilor ai clasei 0
# for pixel in range(784):
#     valori_pixel = train_images[indecsii_imaginilor_clasei_0, pixel]
#     histogram, useless = np.histogram(valori_pixel, limite_interval)
#     print(pixel, " ", histogram/sum(histogram))


# calculam P(Xj = xj |c = i) in matricea m ijk = P(ak <= xj <= bk| c = i)
nr_clase = len(np.unique(train_labels))     #10
nr_pixeli = 784
nr_intervale = 4
m = np.zeros((nr_clase, nr_pixeli, nr_intervale))

for i in range(nr_clase):
    indecsii_imaginilor_clasei_i = np.ravel(np.where(train_labels == i))
    for pixel in range(nr_pixeli):
        valori_pixel = train_images[indecsii_imaginilor_clasei_i, pixel]
        histogram, useless = np.histogram(valori_pixel, limite_interval)
        m[i][pixel] = histogram/sum(histogram)
        # m[i, pixel, :] = histogram/sum(histogram)

m = m + 0.000000001
# print(m.shape)
# test pt pixelul 370
# print("Prob pt valorile pixelului 370 al tuturor img de clasa 0", m[0, 370, :])


# print(np.digitize(62,[0,64,128,192,256]))

# clasificator Naive Bayes care alege clasa cu max prob
# clasa = P(c / X) = argmax(sum(log( P(Xj = xj |c = i))) + sum(log(P(c = i)))
# m ijk = P(ak <= xj <= bk| c = i)


def clasifica_naive_bayes(img, m, limite_interval, p_C):
    nr_clase = len(p_C)
    nr_pixeli = len(img)
    scor_C_img = np.zeros(nr_clase)     # vector cu P(X/c) = scorul pt fiecare clasa
    for clasa in range(0, nr_clase):
        scor_C_img[clasa] = np.log(p_C[clasa])   # log(P(c = i))
        for pixel_idx in range(0, nr_pixeli):
            pixel_curent = img[pixel_idx]   # valoarea pixelului curent
            hist_idx = np.digitize(pixel_curent, limite_interval) - 1   # in ce interv din histograma este valoarea pixelul curent
            prob_pixel_curent = np.log(m[clasa, pixel_idx, hist_idx])   # prob pt valorile pixelului curent (din intervalul determinat) al tuturor img din clasa
            scor_C_img[clasa] += prob_pixel_curent      # log( P(Xj = xj |c = i))
    clasa_prezisa = scor_C_img.argmax()    # alegem clasa cu max prob
    return clasa_prezisa


print("Prima img de test are eticheta: ", clasifica_naive_bayes(test_images[0, :], m, limite_interval, p_C))


# Clasificati toate ex de testare

# print(test_images.shape)
nr_imagini_test, nr_pixeli_imagini_test = test_images.shape
etichete_prezise = []
for nr_imagine in range(0, nr_imagini_test):
    etichete_prezise.append(clasifica_naive_bayes(test_images[nr_imagine, :], m, limite_interval, p_C))
# print(etichete_prezise)


# Calcul matrice de confuzie
def construire_matrice_confuzie(what_is_true, etichete_prezise):
    etichete_prezise = np.array(etichete_prezise)
    what_is_true = np.array(what_is_true)
    matrice_confuzie = np.zeros((nr_clase, nr_clase))   # ((10,10))
    for adevarat, prezis in zip(what_is_true, etichete_prezise):
        matrice_confuzie[adevarat][prezis] += 1
    return matrice_confuzie


matrice_confuzie = construire_matrice_confuzie(test_labels, etichete_prezise)
print("Matrice confuzie \n", matrice_confuzie)

# Acuratetea = cate etichete prezise sunt egale cu ce e adevarat?
def acuratete(etichete_prezise, what_is_true):
    etichete_prezise = np.array(etichete_prezise)
    what_is_true = np.array(what_is_true)

    boolean_index = (etichete_prezise == what_is_true)  # vector cu true/false. Pastreaza doar True

    return len(what_is_true[boolean_index]) * 100 / len(what_is_true)

    # metoda 2
    # accuracy = (etichete_prezise == what_is_true).astype('int').mean() #cate sunt corecte fata de cate sunt in total
    # return accuracy * 100

print("Acuratetea pe mult de testare este: ", acuratete(etichete_prezise, test_labels))     # 80.8


# Care sunt ex de testare cu cele mai multe misclasificari in matr confuzie?
nr_max_ex_misclasificate = -1
clase_misclasificate = []
l, c = matrice_confuzie.shape
# calc nr max de ex misclasificate
for adevarat in range(0, l):
    for prezis in range(0, c):
        if adevarat != prezis and matrice_confuzie[adevarat][prezis] > nr_max_ex_misclasificate:
            nr_max_ex_misclasificate = matrice_confuzie[adevarat][prezis]
# daca sunt mai multe clase cu nr max de misclasificari, le salvam
for adevarat in range(0, l):
    for prezis in range(0, c):
        if adevarat != prezis and matrice_confuzie[adevarat][prezis] == nr_max_ex_misclasificate:
            clase_misclasificate.append((adevarat, prezis))

print("Nr maxim de ex misclasificate: ", nr_max_ex_misclasificate)
print("Clase misclasificate: ", clase_misclasificate)
# print(type(clase_misclasificate))
# print(clase_misclasificate[0][0])


# plotare ex misclasificate
def plot_misclasificare(true_class, bad_class, etichete_prezise, test_images, test_labels):
    nr_imagini_test, nr_pixeli_imagini_test = test_images.shape

    for index in range(0, nr_imagini_test):
        img = test_images[index, :]
        adevarat = test_labels[index]
        prezis = etichete_prezise[index]

        if adevarat == true_class and prezis == bad_class:
            plt.figure(figsize=(1, 1))
            plt.axis('off')
            plt.imshow(np.reshape(img, (28, 28)), cmap="gray")
            plt.show()


for index in range(0, len(clase_misclasificate)):
    true_class = clase_misclasificate[index][0]
    bad_class = clase_misclasificate[index][1]
    plot_misclasificare(true_class, bad_class, etichete_prezise, test_images, test_labels)

